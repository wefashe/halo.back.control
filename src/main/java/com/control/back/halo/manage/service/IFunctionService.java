package com.control.back.halo.manage.service;

import java.util.List;

import com.control.back.halo.basic.entity.vo.TreeNode;
import com.control.back.halo.basic.service.IBaseService;
import com.control.back.halo.manage.entity.Function;

public interface IFunctionService extends IBaseService<Function, Long> {

    Integer maxFunctionLevel(Integer type, Long parentId);

    List<TreeNode> loadTree();

    List<Function> loadFunctionByAdminId(Long adminId);
}
