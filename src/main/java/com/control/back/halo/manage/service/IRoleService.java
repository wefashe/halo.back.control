package com.control.back.halo.manage.service;

import java.util.List;
import java.util.Set;

import com.control.back.halo.basic.entity.vo.TreeNode;
import com.control.back.halo.basic.service.IBaseService;
import com.control.back.halo.manage.entity.Function;
import com.control.back.halo.manage.entity.Role;

public interface IRoleService extends IBaseService<Role, Long> {

    void saveOrUpdate(Role role);

    /**
     * 加载tree并且标记角色所属菜单
     * 
     * @param roleId
     * @return
     */
    List<TreeNode> loadTreeAndMarkRoleFunctions(Long roleId);

    Boolean saveRoleFunctions(Long roleId, Set<Function> functions);
}
