package com.control.back.halo.basic.utils;

import org.joda.time.base.AbstractInstant;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.SimpleDate;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

@SuppressWarnings("deprecation")
public class FreemarkerObjectWrapper extends DefaultObjectWrapper {
    
    @Override
    public TemplateModel wrap(final Object obj) throws TemplateModelException {
        if (obj instanceof AbstractInstant) return new SimpleDate(((AbstractInstant) obj).toDate(), TemplateDateModel.DATETIME);

        return super.wrap(obj);
    }
}
